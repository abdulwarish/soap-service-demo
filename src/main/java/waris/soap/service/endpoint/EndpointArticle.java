package waris.soap.service.endpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.waris.gs_ws.ArticleInfo;
import com.waris.gs_ws.GetAllArticlesResponse;

import waris.soap.service.entity.Article;
import waris.soap.service.repository.ArticleRepository;
@Endpoint
public class EndpointArticle {
	
	@Autowired
	private ArticleRepository articleRepository;
	
	private static final String NAMESPACE_URI = "http://www.waris.com/article-ws";
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllArticlesRequest")
	@ResponsePayload
	public GetAllArticlesResponse getAllArticles() {
		GetAllArticlesResponse response = new GetAllArticlesResponse();
		List<ArticleInfo> collect = articleRepository.findAll().stream().map(EndpointArticle::convert).collect(Collectors.toList());
		response.getArticleInfo().addAll(collect);
		return response;
	}
	
	static ArticleInfo convert(Article art) {
		ArticleInfo articleInfo = new ArticleInfo();
		articleInfo.setArticleId(art.getArticleId());
		articleInfo.setCategory(art.getCategory());
		articleInfo.setTitle(art.getTitle());
		return articleInfo;
	}

}
